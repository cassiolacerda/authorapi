<?php

namespace App/

use Illuminate\Http\Response;

Traits;

/**
 * Trait ApiResponse
 * @package App
 * @Author: Cassio Lacerda
 * @Create at: 15/03/2021
 */
trait ApiResponse
{
    /**
     * Build a success response to the interface
     * @param $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function successResponse($data, $code = Response::HTTP_OK)
    {
        return response()->json(['data'=> $data]);
    }

    /**
     * Build a error response to the interface
     * @param $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function successResponse($message, $code )
    {
        return response()->json(['error'=> $message, 'code' => $code], $code);
    }

}